<?php
/**
 * Implements hook_menu().
 */
function node2fs_menu() {
  $items = array();
  $items['admin/config/node_to_fs'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => t('Node2FS'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node2fs_config'),
    'access arguments' => array('administer nodes')
  );
  $items['admin/config/node_to_fs/restore'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => t('Node2FS Restore'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node2fs_list_nodes'),
    'access arguments' => array('administer nodes')
  );
  $items['admin/config/node_to_fs/restore/%'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => t('Node2FS Restore'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node2fs_view_one'),
    'access arguments' => array('administer nodes')
  );
  return $items;
}

/**
 * Implements hook_cron().
 */
function node2fs_cron() {
  node2fs_maintenance();
}

function node2fs_maintenance() {
  $count = node2fs_get_count();
  $files = node2fs_list_files();
  $files_for_delete = array_slice($files, $count);
  $path = node2fs_get_path();
  if (is_array(($files_for_delete))) {
    foreach ($files_for_delete as $fname) {
      unlink($path.$fname);
    }
  }
}

function node2fs_list_nodes() {
  $files = node2fs_list_files();
  $dir = node2fs_get_path();
  $nodes = array();
  if (is_array($files)) {
    foreach ($files as $file_name) {
      $stored_node = file_get_contents($dir . $file_name);
      $status = array();
      $color_nid='#ADFFB8';
      if ($stored_node) {
        $stored_node = unserialize($stored_node);
        $stored_node_modified = filemtime($dir . $file_name);
        $db_info = node2fs_get_node_by_uuid($stored_node->uuid);
        $db_modified = 0;
        if (isset($db_info['changed'])) {
          $db_modified = $db_info['changed'];
        }
        $delta = $stored_node_modified - $db_modified;
        $color_db = $color_stored = '#ADFFB8';
        if (abs($delta) < 5) {
          $delta = 0;
          $status []= t('Synced!');
        }
        elseif ($delta > 0) {
          $delta = 1;
          $color_db = '#FFADB7';
          $status []= t('Stored is newer');
        }
        else {
          $delta = -1;
          $color_stored = '#FFADB7';
          $status []= t('Db is newer');
        }

        $line = array(
          implode('|',$status),
          array('data' => $stored_node->nid, 'style' => 'background-color:' . $color_nid . ';'),
          array('data' => l($db_info['title'],'node/'.$stored_node->nid), 'style' => 'background-color:' . $color_nid . ';'),
          array('data' => $stored_node->title, 'style' => 'background-color:' . $color_nid . ';'),
          array('data' => date('c',$db_modified), 'style' => 'background-color:' . $color_db . ';'),
          array('data' => date('c', $stored_node_modified), 'style' => 'background-color:' . $color_stored . ';'),
        );
        $nodes[$stored_node->uuid] = $line;
      }
    }
  }
  $header = array(
    'Status',
    'Nid',
    'Db Title',
    'Stored Title',
    'DB Changed',
    'Stored Changed'
  );

  $form['nodes'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $nodes
  );

  $form['revert']=array(
    '#type'=>'submit',
    '#value' => 'Stored -> DB'
  );

  $form['cleanup']=array(
    '#type'=>'submit',
    '#value' => 'DB -> Stored'
  );
  return $form;
}

/**
 * @param $uuid
 *
 * @return mixed
 */
function node2fs_get_node_by_uuid($uuid) {
  $db_info = db_select('node', 'n')->fields('n', array(
    'nid',
    'title',
    'changed',
    'uuid'
  ))->condition('n.uuid', $uuid, '=')->execute()->fetchAssoc();
  return $db_info;
}

function node2fs_list_nodes_submit($form,$form_state){
  $uuids =array();
  foreach($form_state['values']['nodes'] as $val){
    if($val) {
      $uuids[$val]=$val;
    }
  }

  foreach($uuids as $uuid){
    $db_node_inf = node2fs_get_node_by_uuid($uuid);
    if($form_state['values']['op'] == 'Stored -> DB'){
      $dir = node2fs_get_path();
      $node = file_get_contents($dir.$uuid.'.ser');
      if($node){

        $node = unserialize($node);
        if(!$db_node_inf) {
          unset($node->nid);
          unset($node->vid);
        }
        node_save($node);
      }
    }
    else{
      $node = node_load($db_node_inf['nid']);
      node_save($node);
    }
  }
}

function node2fs_get_enabled_types() {
  $types = variable_get('n2fs_enabled_types', array());
  $res = array();
  foreach ($types as $val) {
    if ($val) {
      $res[$val] = $val;
    }
  }
  return $res;
}


function node2fs_list_files() {
  $hash_folder = node2fs_get_path();
  $dir = opendir($hash_folder);
  $list = array();
  while ($file = readdir($dir)) {
    if ($file != '.' and $file != '..') {
      $ctime = filemtime($hash_folder . $file) . ',' . $file;
      $list[$ctime] = $file;
    }
  }
  closedir($dir);
  krsort($list);
  return $list;
}

function node2fs_config() {
  $form = array();
  $nt = node_type_get_types();
  $options = array();
  $hash_folder = variable_get('n2fs_foldername', '');
  if (!$hash_folder) {
    $hash_folder = md5('qef' . time() . 'node' . rand(0, 1000));
  }
  $form['n2fs_foldername'] = array(
    '#title' => t('Folder to store nodes'),
    '#type' => 'textfield',
    '#default_value' => $hash_folder
  );
  $form['n2fs_count'] = array(
    '#title' => t('Maximum count of stored nodes'),
    '#type' => 'textfield',
    '#default_value' => node2fs_get_count()
  );
  foreach ($nt as $key => $val) {
    $options[$key] = $val->name;
  }
  $form['n2fs_enabled_types'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => variable_get('n2fs_enabled_types', array()),
    '#title' => t('Types to store')
  );
  return system_settings_form($form);
}


function node2fs_store($node) {
  $types = node2fs_get_enabled_types();
  if (isset($types[$node->type])) {
    $serialized = serialize($node);
    $n2fs_dir = node2fs_get_path();
    if (!file_exists($n2fs_dir)) {
      mkdir($n2fs_dir, 0755, TRUE);
    }
    $file2store = fopen($n2fs_dir . '/' . $node->uuid . '.ser', 'w');
    fwrite($file2store, $serialized);
    fclose($file2store);
  }
}

/**
 * @return string
 */
function node2fs_get_count() {
  return variable_get('n2fs_count',500);
}

/**
 * @return string
 */
function node2fs_get_path() {
  if ($wrapper = file_stream_wrapper_get_instance_by_uri('public://')) {
    $realpath = $wrapper->realpath();
  }
  $hash_folder = variable_get('n2fs_foldername', '');
  $n2fs_dir = $realpath . '/' . $hash_folder . '/';
  return $n2fs_dir;
}

/**
 * Implements hook_insert().
 */
function node2fs_node_insert($node) {
  node2fs_store($node);
}

/**
 * Implements hook_update().
 */
function node2fs_node_update($node) {
  $loaded_node = node_load($node->nid, NULL, TRUE);
  node2fs_store($loaded_node);
}

